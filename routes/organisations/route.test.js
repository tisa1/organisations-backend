const mongoose = require("mongoose")
const { Org } = require("db/organisations/collection")
const app = require("middleware/app")
const { responses } = require("db/organisations/constants")
const config = require("config")
const User = require("db/users/collection")
const supertest = require("supertest")

const orgRouter = require("routes/organisations/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")
const { OrgMembership } = require("db/org-memberships/collection")
const { roles } = require("db/org-memberships/constants")

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const data = {
  name: "Tisa",
}

const login = (user) => jwt.sign(
  { email: user.email, userId: user?._id },
  config.jwt.secret,
  {
    expiresIn: config.jwt.expiresIn,
  },
)

describe("organisations route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Org.deleteMany({})
    await OrgMembership.deleteMany({})
    await User.deleteMany({})
  })

  it("creating an org with valid data and authentication should work", async () => {
    app.use("/", orgRouter)

    const user = await new User(userData).save()

    const token = jwt.sign(
      { email: userData.email, userId: user?._id },
      config.jwt.secret,
      {
        expiresIn: config.jwt.expiresIn,
      },
    )

    const res = await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token}`)
      .expect(200)

    expect(res.body.message).toBe(responses.ORGANISATION_CREATED)
    expect(res.body.message).not.toBe(undefined)
  })

  it("creating an org without a token should not work", async () => {
    app.use("/", orgRouter)

    const res = await request.post("/create")
      .send(data)
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
    expect(res.body.message).not.toBe(undefined)

  })

  it("creating an org with an invalid token should not work", async () => {
    app.use("/", orgRouter)

    const res = await request.post("/create")
      .send(data)
      .set("Authorization", "Bearer XXXXXX")
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
    expect(res.body.message).not.toBe(undefined)

  })

  it("creating an org without a name should not work", async () => {
    app.use("/", orgRouter)

    const user = await new User(userData).save()

    const token = jwt.sign(
      { email: userData.email, userId: user?._id },
      config.jwt.secret,
      {
        expiresIn: config.jwt.expiresIn,
      },
    )

    const orgData = _.clone(data)
    delete orgData.name

    const res = await request.post("/create")
      .send(orgData)
      .set("Authorization", `Bearer ${token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_NAME)
    expect(res.body.message).not.toBe(undefined)

  })

  it("creating 2 organisations with the same name should fail", async () => {
    app.use("/", orgRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    // creating 2 companies for 1 user and 2 companies for another one
    await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    const res2 = await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res2.body.message).toBe(responses.NAME_TAKEN)
    expect(res.body.message).not.toBe(undefined)

  })

  it("a user should have access to his organisations only", async () => {
    app.use("/", orgRouter)

    const user1 = await new User(userData).save()
    const anotherUser = _.extend(userData, { email: "anotherUser@app.com" })
    const user2 = await new User(anotherUser).save()

    const token1 = login(user1)
    const token2 = login(user2)

    // creating 2 companies for 1 user and 2 companies for another one
    await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    const anotherOrg = _.clone(data)
    anotherOrg.name = "Tisa2"

    await request.post("/create")
      .send(anotherOrg)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    anotherOrg.name = "Tisa3"
    await request.post("/create")
      .send(anotherOrg)
      .set("Authorization", `Bearer ${token2}`)
      .expect(200)

    anotherOrg.name = "Tisa4"
    await request.post("/create")
      .send(anotherOrg)
      .set("Authorization", `Bearer ${token2}`)
      .expect(200)

    const res1 = await request.get("/me")
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    const res2 = await request.get("/me")
      .set("Authorization", `Bearer ${token2}`)
      .expect(200)

    // each user needs to get his organisations
    expect(res1.body.total).toBe(2)
    expect(res2.body.total).toBe(2)
  })

  it("valid deletion of an organisation should work", async () => {
    app.use("/", orgRouter)

    const user = await new User(userData).save()
    const token = login(user)

    // creating 2 companies for 1 user and 2 companies for another one
    const res = await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token}`)
      .expect(200)

    const org = res.body.organisation

    await request.delete("/single")
      .send({ organisationId: org._id })
      .set("Authorization", `Bearer ${token}`)
      .expect(200)

    const orgMemberships = await OrgMembership.countDocuments()
    const orgs = await Org.countDocuments()
    expect(orgMemberships).toBe(0)
    expect(orgs).toBe(0)
  })

  it("a user should not be able to delete someone else's organisation", async () => {
    app.use("/", orgRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const user2 = await new User({ email: "another@app.com", ...userData }).save()
    const token2 = login(user2)

    // creating 2 companies for 1 user and 2 companies for another one
    await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    const orgRes = await request.get("/me")
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    const org = orgRes.body.organisations[0]

    await request.delete("/single")
      .send({ organisationId: org._id })
      .set("Authorization", `Bearer ${token2}`)
      .expect(403)
  })

  it("valid deletion of an organisation should also delete the corresponding memberships", async () => {
    app.use("/", orgRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const user2 = await new User({ email: "another@app.com", ...userData }).save()

    const res1 = await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)
    const org1 = res1.body.organisation

    const res2 = await request.post("/create")
      .send({ name: "Tisa 2" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)
    const org2 = res2.body.organisation

    let orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(2)

    // 2 memberships are also created but we will grant another 2 membership to another user
    await new OrgMembership({ userId: user2._id, orgId: org1._id, role: roles.MEMBER }).save()
    await new OrgMembership({ userId: user2._id, orgId: org2._id, role: roles.MEMBER }).save()

    orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(4)

    await request.delete("/single")
      .send({ organisationId: org1._id })
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    await request.delete("/single")
      .send({ organisationId: org2._id })
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(0)
  })

  it("creating 2 organisations should create 2 memberships automatically", async () => {
    app.use("/", orgRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    await request.post("/create")
      .send({ name: "Tisa 2" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    const orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(2)
  })

  it("deletion of an organisation should also delete the corresponding memberships but leave the other memberships alone", async () => {
    app.use("/", orgRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const user2 = await new User({ email: "another@app.com", ...userData }).save()

    const res1 = await request.post("/create")
      .send(data)
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)
    const org1 = res1.body.organisation

    const res2 = await request.post("/create")
      .send({ name: "Tisa 2" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)
    const org2 = res2.body.organisation

    let orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(2)

    // 2 memberships are also created but we will grant another 2 membership to another user
    await new OrgMembership({ userId: user2._id, orgId: org1._id, role: roles.MEMBER }).save()
    await new OrgMembership({ userId: user2._id, orgId: org2._id, role: roles.MEMBER }).save()

    // there should be 4 now
    orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(4)

    await request.delete("/single")
      .send({ organisationId: org1._id })
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    orgMemberships = await OrgMembership.countDocuments()
    expect(orgMemberships).toBe(2)
  })
})
