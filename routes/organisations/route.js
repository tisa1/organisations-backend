const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

const { Org } = require("db/organisations/collection")
const { OrgMembership } = require("db/org-memberships/collection")
const jwtSecurity = require("core/jwt")
const { responses } = require("db/organisations/constants")
const { roles } = require("db/org-memberships/constants")

const _ = require("underscore")
const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const orgRouter = express.Router()

  orgRouter.post("/create", [
    jwtSecurity.checkToken,
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const userId = req?.decoded?.userId
      const data = req?.body

      const organisation = await new Org(data).save()
      await new OrgMembership({ userId, orgId: organisation._id, role: roles.OWNER }).save()

      return res.json({ message: responses.ORGANISATION_CREATED, organisation })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on organisations /create route",
      })
    }
  })

  orgRouter.get("/me", [
    jwtSecurity.checkToken,
    routeSecurity.me,
  ], async (req, res, next) => {
    try {
      const userId = req?.decoded?.userId
      const projection = {
        name: 1,
        createdAt: 1,
        organisation: 1,
      }

      const organisations = await OrgMembership.aggregate([
        {
          $match: {
            userId: mongoose.Types.ObjectId(userId),
          },
        },
        {
          $lookup: {
            from: "organisations",
            let: { orgId: "$orgId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$orgId"] } } },
              {
                $project: {
                  _id: 1,
                  name: 1,
                },
              },
            ],
            as: "organisation",
          },
        },
        {
          $addFields: { organisation: { $arrayElemAt: ["$organisation", 0] } },
        },
        {
          $project: projection,
        },
      ])
      const total = await OrgMembership.countDocuments({ userId })

      return res.json({ organisations, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on organisations /me route",
      })
    }
  })

  orgRouter.delete("/single", [
    jwtSecurity.checkToken,
    routeSecurity.remove,
  ], async (req, res, next) => {
    try {
      const organisationId = req?.body?.organisationId

      await Org.deleteOne({ _id: organisationId })
      await OrgMembership.deleteMany({ orgId: organisationId })

      return res.json({ message: responses.ORGANISATION_DELETED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on organisations /me route",
      })
    }
  })

  return orgRouter
}

module.exports = createRouter
