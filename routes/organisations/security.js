const mongoose = require("mongoose")
const User = require("db/users/collection")
const { Org } = require("db/organisations/collection")
const { responses } = require("db/organisations/constants")
const { roles } = require("db/org-memberships/constants")
const { OrgMembership } = require("db/org-memberships/collection")

const create = async (req, res, next) => {
  const userId = req?.decoded?.userId
  if (!userId) {
    return res.status(400).json({ message: responses.NO_USER_ID })
  }
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({ message: responses.USER_ID_INVALID })
  }
  const existingUser = await User.findById(userId, { _id: 1 })
  if (!existingUser) {
    return res.status(400).json({ message: responses.NO_USER_FOUND })
  }

  const name = req?.body?.name
  if (!name) {
    return res.status(400).json({ message: responses.NO_NAME })
  }

  const existingOrg = await Org.findOne({ name })

  if (existingOrg) {
    return res.status(400).json({ message: responses.NAME_TAKEN })
  }

  return next()
}

const me = async (req, res, next) => {
  const userId = req?.decoded?.userId

  if (!userId) {
    return res.status(400).json({ message: responses.NO_USER_ID })
  }
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({ message: responses.USER_ID_INVALID })
  }
  const existingUser = await User.findById(userId, { _id: 1 })
  if (!existingUser) {
    return res.status(400).json({ message: responses.NO_USER_FOUND })
  }

  return next()
}

const remove = async (req, res, next) => {
  const userId = req?.decoded?.userId

  if (!userId) {
    return res.status(400).json({ message: responses.NO_USER_ID })
  }
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({ message: responses.USER_ID_INVALID })
  }
  const existingUser = await User.findById(userId, { _id: 1 })
  if (!existingUser) {
    return res.status(400).json({ message: responses.NO_USER_FOUND })
  }

  const organisationId = req?.body?.organisationId
  if (!organisationId) {
    return res.status(400).json({ message: responses.NO_ORGANISATION_ID })
  }

  const isUserAbleToDelete = await OrgMembership.countDocuments({
    userId, orgId: organisationId, role: roles.OWNER,
  })

  if (!isUserAbleToDelete) {
    return res.status(403).json({ message: responses.NOT_ALLOWED })
  }

  return next()
}

module.exports = {
  create,
  me,
  remove,
}
