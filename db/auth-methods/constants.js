const authMethodTypes = {
  PASSWORD: "password",
}

const responses = {
  EMAIL_ALREADY_USED: "emailAlreadyUsed",
  ACCOUNT_CREATED: "accountCreated",
  NO_EMAIL: "noEmail",
  EMAIL_INVALID: "emailInvalid",
  NO_PASSWORD: "noPassword",
  PASSWORD_WEAK: "weakPassword",
  PASSWORD_WRONG: "wrongPassword",
  ACCOUNT_NOT_FOUND: "accountNotFound",
  SIGNIN_SUCCESS: "signInSuccess",
}

module.exports = {
  authMethodTypes,
  responses,
}
