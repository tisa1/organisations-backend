const mongoose = require("mongoose")
const { OrgSchema } = require("./schema")

const Org = mongoose.model("organisations", OrgSchema)

module.exports = {
  Org,
}
